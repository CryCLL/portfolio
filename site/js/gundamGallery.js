console.log("script loaded");
$("a").mouseenter(function() {
  // This function implements the Fade To capability.
  console.log("$(this):" + $(this));
    $(this).fadeTo( 200,0.6, function() {
    console.log("Fade To complete!");
  });
});

$("a").mouseleave(function() {
  // This function implements the Fade To capability.
  console.log("$(this):" + $(this));
    $(this).fadeTo( 200,1, function() {
    console.log("Fade To complete!");
  });
});
